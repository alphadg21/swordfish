package com.inleague.swordfish;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.fima.cardsui.views.CardUI;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */


    public static final String ARG_SECTION_NUMBER = "0";

    MapView m;
    Context context;

    public MapFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.mapfrag, container, false);



        GoogleMapOptions options = new GoogleMapOptions();
        options.mapType(GoogleMap.MAP_TYPE_HYBRID)
                .compassEnabled(true)
                .rotateGesturesEnabled(false)
                .tiltGesturesEnabled(false);


        m=new MapView(getActivity(), options);
        m=(MapView) rootView.findViewById(R.id.mapView);
        m.onCreate(savedInstanceState);



        final GoogleMap myMap;
        myMap=m.getMap();
        myMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        myMap.setMyLocationEnabled(true);

        LocationManager lm=(LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria crit=new Criteria();
        crit.setAccuracy(Criteria.ACCURACY_COARSE);
        String prov=lm.getBestProvider(crit, true);
        Location myLocation=lm.getLastKnownLocation(prov);
        if(myLocation!=null){
            try {
                MapsInitializer.initialize(getActivity());
            } catch (GooglePlayServicesNotAvailableException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            myMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.getLatitude(),myLocation.getLongitude()), 12));
        }
        try {
            InputStream inputStream = getActivity().getAssets().open("players.xml");

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db;

            db = dbf.newDocumentBuilder();

            Document doc = db.parse(inputStream);

            doc.getDocumentElement().normalize();
            NodeList nodeLst = doc.getElementsByTagName("player");

            new AsyncMap(rootView).execute(nodeLst);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return rootView;
    }
    @Override
    public void onResume() {
        super.onResume();

        m.onResume();

    }
    @Override
    public void onPause() {
        super.onPause();
        m.onPause();

    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        m.onDestroy();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        m.onLowMemory();

    }








    public class AsyncMap extends AsyncTask<NodeList, Integer, ArrayList<MarkerOptions>>{
        private View rootView;
        MapView m;
        public AsyncMap(View rootView){
            this.rootView=rootView;
        }


        @Override
        protected ArrayList<MarkerOptions> doInBackground(NodeList... parameter) {
            ArrayList<Business> theBusiness=new ArrayList<Business>();
            ArrayList<MarkerOptions> out=new ArrayList<MarkerOptions>();
            NodeList nodeLst = parameter[0];
            final ArrayList<Athlete> toUse=readXml(nodeLst);

            Geocoder myCodes=new Geocoder(getActivity());
            LocationManager lm=(LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
            try {
                Location mine=lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                List<Address> imHere=myCodes.getFromLocation(mine.getLatitude(), mine.getLongitude(), 1);
                Address local=imHere.get(0);
                ArrayList<String> owners=new ArrayList<String>();
                int q=0;
                while(q<toUse.size()){
                    int p=0;
                    while(p<toUse.get(q).biznum){
                        if(!toUse.get(q).getBusiness(p).getAddress1().equals(" ")){
                            if(toUse.get(q).getBusiness(p).getCity().equals(local.getLocality())){
                                theBusiness.add(toUse.get(q).getBusiness(p));
                                owners.add(toUse.get(q).getName());
                            }

                            p++;
                        }
                        else {
                            p++;
                        }
                    }
                    q++;
                }

                GoogleMapOptions options = new GoogleMapOptions();
                options.mapType(GoogleMap.MAP_TYPE_HYBRID)
                        .compassEnabled(true)
                        .rotateGesturesEnabled(false)
                        .tiltGesturesEnabled(false);
                m=new MapView(getActivity(), options);
                m=(MapView) rootView.findViewById(R.id.mapView);
                GoogleMap myMap;
                myMap=m.getMap();
                int r=0;
                // ArrayList<String> info=new ArrayList<String>();
                // ArrayList<LatLng> spots=new ArrayList<LatLng>();

                while(r<theBusiness.size()){
                    String strAddress=theBusiness.get(r).getAddress1()+" "+
                            theBusiness.get(r).getAddress2()+"\n"+
                            theBusiness.get(r).getCity()+" "+
                            theBusiness.get(r).getState()+" "+
                            theBusiness.get(r).getZip();

                    String theName=theBusiness.get(r).getName();
                    List<Address> address;

                    address = myCodes.getFromLocationName(strAddress,1);
                    if(address.size()>0){
                        Address location = address.get(0);

                        if(mine!=null){


                            LocationManager mySpot=(LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
                            Criteria crit=new Criteria();
                            crit.setAccuracy(Criteria.ACCURACY_COARSE);
                            String prov=mySpot.getBestProvider(crit, true);
                            LatLng mark=new LatLng(location.getLatitude(),location.getLongitude());

                            Location thisPlace = new Location(prov);
                            thisPlace.setLatitude(location.getLatitude());
                            thisPlace.setLongitude(location.getLongitude());
                            int distance;
                            distance=(int)thisPlace.distanceTo(mine);
                            distance=distance/1609;

                            if(distance<25){
                                MarkerOptions put=new MarkerOptions().position(mark).title(theName).snippet(strAddress+"\n"+distance+" miles away\nPlayer: "+owners.get(r));
                                out.add(put);

                            }
                        }
                    }
                    r++;
                }



            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Toast.makeText(getActivity(), "oh shit", Toast.LENGTH_SHORT).show();
            }



            return out;
        }

        protected void onPostExecute(ArrayList<MarkerOptions> theBusiness){
            GoogleMap myMap=m.getMap();
            int s=0;
            MyCard searchbar=new MyCard("Explore Map Of Businesses");
            searchbar.setOnClickListener(new OnClickListener(){

                @Override
                public void onClick(View arg0) {
                    Intent I=new Intent(getActivity(), MapsActivity.class);
                    startActivity(I);

                }
            });
            CardUI myCards=(CardUI)rootView.findViewById(R.id.bizcards);
            myCards.setSwipeable(false);

            myCards.refresh();
            while(s<theBusiness.size()){
                myMap.addMarker(theBusiness.get(s));
                final LocationCard thisOne=new LocationCard(theBusiness.get(s).getTitle(), theBusiness.get(s).getSnippet());
                thisOne.setOnClickListener(new OnClickListener(){

                    @Override
                    public void onClick(View arg0) {
                        String name=thisOne.getTitle();
                        String address=thisOne.getDesc();
                        Intent goBiz=new Intent(getActivity(), BusinessActivity.class);
                        goBiz.putExtra("spot", address);
                        goBiz.putExtra("title", name);
                        startActivity(goBiz);

                    }
                });
                myMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener(){

                    @Override
                    public void onInfoWindowClick(Marker clickedOn) {
                        String name=clickedOn.getTitle();
                        String address=clickedOn.getSnippet();
                        Intent goBiz=new Intent(getActivity(), BusinessActivity.class);
                        goBiz.putExtra("spot", address);
                        goBiz.putExtra("title", name);
                        startActivity(goBiz);

                    }

                });
                myCards.addCard(thisOne);
                s++;


            }
            myCards.addCard(searchbar);
            myCards.refresh();


        }

        private ArrayList<Athlete> readXml(NodeList inputnodelist) {
            // TODO Auto-generated method stub

            try{
                ArrayList<Athlete> returnlist = new ArrayList<Athlete>();
                //Athlete[] returnlist = new Athlete[inputnodelist.getLength()];
                //	Toast.makeText(getApplicationContext(), inputnodelist.getLength() + " ", Toast.LENGTH_SHORT).show();
                int index = 0;
                String player_id;
                String first_name;
                String last_name;
                String player_name;
                String league;
                String team;
                String [] teams_input = new String[1];
                String year;
                String [] years_input = new String[1];
                String twitter;
                String achievement_tags;
                String [] achievements_input = new String[1];
                String business_name;
                String address1;
                String address2;
                String city;
                String state;
                String zip;
                String country;
                String website;
                String business_tags;
                String [] business_tags_input = new String[1];
                String imgUrl;

                for(int i = 0; i<inputnodelist.getLength(); i++)
                {
                    //Toast.makeText(getApplicationContext(), "Made it inside the FOR Loop", Toast.LENGTH_SHORT).show();
                    // Set node for this iteration
                    Node athNode = inputnodelist.item(i);
                    if(athNode.getNodeType() == Node.ELEMENT_NODE)
                    {
                        // Cast current node as an element
                        Element curr_element = (Element)athNode;
                        //Retrieve the id_tag and get the value it contains
					/* deprecated
					NodeList id_tag = curr_element.getElementsByTagName("player_id");
					Element id_element = (Element) id_tag.item(0);
					player_id =id_element.toString();
					*/
                        //player_id = curr_element.getElementsByTagName("player_id").item(0).getTextContent();
                        first_name = curr_element.getElementsByTagName("FIRST_NAME").item(0).getTextContent();
                        //Toast.makeText(getApplicationContext(), first_name, Toast.LENGTH_SHORT).show();
                        last_name = curr_element.getElementsByTagName("LAST_NAME").item(0).getTextContent();
                        //Toast.makeText(getApplicationContext(), last_name, Toast.LENGTH_SHORT).show();
                        if(curr_element.getElementsByTagName("LEAGUE").getLength() == 0)
                        {
                            league=" ";
                        }
                        else
                        {
                            league = curr_element.getElementsByTagName("LEAGUE").item(0).getTextContent();
                        }
                        //Toast.makeText(getApplicationContext(), league, Toast.LENGTH_SHORT).show();

                        if(curr_element.getElementsByTagName("TEAM").getLength() == 0)
                        {
                            team = " ";
                            teams_input[0] = " ";
                        }
                        else
                        {
                            team = curr_element.getElementsByTagName("TEAM").item(0).getTextContent();
                            teams_input = team.split(", ");
                        }


                        //Toast.makeText(getApplicationContext(), team, Toast.LENGTH_SHORT).show();

                        if(curr_element.getElementsByTagName("YEARS").getLength() == 0)
                        {
                            year = " ";
                            years_input[0] = " ";
                        }
                        else
                        {
                            year = curr_element.getElementsByTagName("YEARS").item(0).getTextContent();
                            years_input = year.split(", ");
                        }

                        //Toast.makeText(getApplicationContext(), year, Toast.LENGTH_SHORT).show();
                        if(curr_element.getElementsByTagName("TWITTER").getLength() == 0)
                        {
                            twitter=" ";
                        }
                        else{
                            twitter = curr_element.getElementsByTagName("TWITTER").item(0).getTextContent();
                        }
                        if(curr_element.getElementsByTagName("ATHLETE_IMAGE_URL").getLength() == 0)
                        {
                            imgUrl=" ";
                        }
                        else{
                            imgUrl = curr_element.getElementsByTagName("ATHLETE_IMAGE_URL").item(0).getTextContent();
                        }
                        //Toast.makeText(getApplicationContext(), twitter, Toast.LENGTH_SHORT).show();

                        //Toast.makeText(getApplicationContext(), " "+ curr_element.getElementsByTagName("ACHIEVEMENT_Tags").getLength() +" ", Toast.LENGTH_SHORT).show();
                        if(curr_element.getElementsByTagName("ACHIEVEMENT_Tags").getLength() == 0)
                        {
                            achievement_tags = " ";
                            achievements_input[0] = " ";
                            //	Toast.makeText(getApplicationContext(), "IF", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            achievement_tags = curr_element.getElementsByTagName("ACHIEVEMENT_Tags").item(0).getTextContent();
                            achievements_input = achievement_tags.split(", ");
                            //Toast.makeText(getApplicationContext(), "ELSE", Toast.LENGTH_SHORT).show();
                        }
                        //Toast.makeText(getApplicationContext(), achievement_tags, Toast.LENGTH_SHORT).show();
                        Business temp = new Business();
                        if(curr_element.getElementsByTagName("BUSINESS_NAME").getLength() == 0)
                        {
                            business_name= "Name Unknown";
                        }
                        else{
                            business_name = curr_element.getElementsByTagName("BUSINESS_NAME").item(0).getTextContent();
                        }
                        //Toast.makeText(getApplicationContext(), business_name, Toast.LENGTH_SHORT).show();
                        if(curr_element.getElementsByTagName("ADDRESS_1").getLength() == 0)
                        {
                            address1= " ";
                        }
                        else{
                            address1 = curr_element.getElementsByTagName("ADDRESS_1").item(0).getTextContent();
                        }

                        //Toast.makeText(getApplicationContext(), address1, Toast.LENGTH_SHORT).show();
                        if(curr_element.getElementsByTagName("ADDRESS_2").getLength() == 0)
                        {
                            address2= " ";
                        }
                        else{
                            address2 = curr_element.getElementsByTagName("ADDRESS_2").item(0).getTextContent();
                        }
                        //Toast.makeText(getApplicationContext(), address2, Toast.LENGTH_SHORT).show();
                        if(curr_element.getElementsByTagName("CITY").getLength() == 0)
                        {
                            city= " ";
                        }
                        else{
                            city = curr_element.getElementsByTagName("CITY").item(0).getTextContent();
                        }

                        //Toast.makeText(getApplicationContext(), city, Toast.LENGTH_SHORT).show();
                        if(curr_element.getElementsByTagName("STATE").getLength() == 0)
                        {
                            state= " ";
                        }
                        else{
                            state = curr_element.getElementsByTagName("STATE").item(0).getTextContent();
                        }
                        //Toast.makeText(getApplicationContext(), state, Toast.LENGTH_SHORT).show();
                        if(curr_element.getElementsByTagName("ZIP").getLength() == 0)
                        {
                            zip= " ";
                        }
                        else{
                            zip = curr_element.getElementsByTagName("ZIP").item(0).getTextContent();
                        }

                        if(curr_element.getElementsByTagName("COUNTRY").getLength() == 0)
                        {
                            country= " ";
                        }
                        else{
                            country = curr_element.getElementsByTagName("COUNTRY").item(0).getTextContent();
                        }

                        //Toast.makeText(getApplicationContext(), zip, Toast.LENGTH_SHORT).show();
                        if(curr_element.getElementsByTagName("WEBSITE").getLength() == 0)
                        {
                            website= "www.inleague.com";
                        }
                        else{
                            website = curr_element.getElementsByTagName("WEBSITE").item(0).getTextContent();
                        }
                        //Toast.makeText(getApplicationContext(), website, Toast.LENGTH_SHORT).show();
                        if(curr_element.getElementsByTagName("Business_Tags").getLength() == 0)
                        {
                            business_tags= " ";
                        }
                        else{
                            business_tags = curr_element.getElementsByTagName("Business_Tags").item(0).getTextContent();
                        }

                        //Toast.makeText(getApplicationContext(), business_tags, Toast.LENGTH_SHORT).show();
                        //Now we combine the first and last name
                        player_name = first_name.concat(" "+last_name);
                        //Toast.makeText(getApplicationContext(), player_name, Toast.LENGTH_SHORT).show();
                        //and we must parse the strings for teams, years, achievements, and business tags via comma



                        business_tags_input = business_tags.split(", ");
                        //Now we need to dissect the businesses and build Business objects for each of them.
                        ArrayList<Business> businesstopass = new ArrayList<Business> ();
                        //businesstopass.add(new Business(business_name, address1, address2, city, state, zip, ));
                        temp.setName(business_name);
                        temp.setAddress1(address1);
                        temp.setAddress2(address2);
                        temp.setCity(city);
                        temp.setState(state);
                        temp.setZip(zip);
                        temp.setCountry(country);
                        temp.setWebsite(website);
                        temp.setBizTags(business_tags);

                        businesstopass.add(temp);


                        //Toast.makeText(getApplicationContext(), player_name, Toast.LENGTH_SHORT).show();
                        //now that all that crap is done, we can create the athlete object
                        Athlete guy=new Athlete(player_name, league, teams_input, years_input, twitter, achievements_input, businesstopass, imgUrl);
                        int o=0;
                        int j=0;
                        int a=0;
                        while(o<returnlist.size()){
                            if(returnlist.get(o).equals(guy)){
                                j++;
                                a=o;
                            }
                            o++;
                        }
                        if(j == 0){
                            returnlist.add(guy);
                        }
                        else{
                            returnlist.get(a).addBusiness(temp);
                        }
                        //returnlist[i] = new Athlete(player_name, league, teams_input, years_input, twitter, achievements_input, businesstopass);
                        //if(i >= 30)
                        //Toast.makeText(getApplicationContext(), player_name + i, Toast.LENGTH_SHORT).show();


                    }



                }

                return returnlist;



            }
            catch(Exception e)
            {
                throw new RuntimeException("Error occured during reading Athlete Data",e);
            }



        }





        private void executeAsync() {
            /**
             * Listing 9-12: Executing an asynchronous task
             */
            String input = "Charles...Charles...Charles...Charles...Charles...";
            try{



            }catch(Exception e)
            {
                e.printStackTrace();
            }


            // new MyAsyncTask().execute(input);
        }
    }
}