package com.inleague.swordfish;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class TeamListView extends Activity{
	

		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.teamlist);
	        
			getActionBar().setDisplayHomeAsUpEnabled(true);
	        try{
	                        
	                        
	                        
	                        InputStream inputStream =getAssets().open("players.xml");

	                        
	                        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	                        DocumentBuilder db =dbf.newDocumentBuilder();
	                        Document doc = db.parse(inputStream);
	                        
	                        doc.getDocumentElement().normalize();
	                        NodeList nodeLst = doc.getElementsByTagName("player");
	                        ArrayList<Athlete> theathletes = readXml(nodeLst);
	                        String team=getIntent().getStringExtra("team");
	                        String year=getIntent().getStringExtra("year");
	                        final ArrayList<Athlete> thisTeam = new ArrayList<Athlete>();
	                        ArrayList<String> teamNames=new ArrayList<String>();
	                        ListView thisTeamList=(ListView)findViewById(R.id.teamlist);
	                        ArrayAdapter<String> adaptTeam=new ArrayAdapter<String>(this, R.layout.list_item, teamNames);
	                        thisTeamList.setAdapter(adaptTeam);
	                      
	                        int m=0;
	                        while(m<theathletes.size()){
	                        	
	                        	Athlete draftHim=theathletes.get(m);
	                        	String[] compareTeams=draftHim.getTeams();
	                        	String[] yearsSet=draftHim.getYears();
	                        	int t=0;
	                        	
	                        	if(!compareTeams.equals(" ") & !yearsSet.equals(" ")){
	                        	while(t<compareTeams.length){
	                        		
	                        		if(compareTeams[t].equals(team)){
	                        			TeamYear collectThis=new TeamYear(compareTeams[t],yearsSet[t]);
	    	                        	int[] yearsCheck=collectThis.getYearsNos();
	    	                        	int q=0;
	    	                        	while(q<yearsCheck.length){ 

	    	                        		if(yearsCheck[q]==Integer.parseInt(year)){
	    	                        			thisTeam.add(draftHim);
	    	                        			teamNames.add(draftHim.getName());
	    	                        			adaptTeam.notifyDataSetChanged();
	                        				}
	    	                        		q++;
	    	                        	}
	                        		}
	                        		t++;    	
	                        	}
	                        	}	
	                        	m++;		
	                        		
	                        	}
	                        	
	                        	
	                        	
	                        
	                        
	                        thisTeamList.setOnItemClickListener(new OnItemClickListener(){
	        					
	        					
	        					
	        					@Override
	        					public void onItemClick(AdapterView<?> arg0, View arg1,
	        							int arg2, long arg3) {
	        						Athlete thisGuy=thisTeam.get(arg2);
	        						String nametoUse=thisGuy.getName();
	        						String leaguetoUse=thisGuy.getLeague();
	        						String[] teamstoUse=thisGuy.getTeams();
	        						String[] yearstoUse=thisGuy.getYears();
	        						String[] bizzes=new String[thisGuy.biznum];
	        						String[] webSites=new String[thisGuy.biznum];
	        						String twitURL=thisGuy.getTwitter();
	        						String imgAddy=thisGuy.getImageUrl();
	        						int q = 0;
	        						while(q<bizzes.length){
	        							bizzes[q]=thisGuy.getBusiness(q).toString();
	        							webSites[q]=thisGuy.getBusiness(q).website;
	        							q++;
	        						}
	        						
	        						Intent i=new Intent(TeamListView.this, AthleteActivity.class);
	        						i.putExtra("aName", nametoUse);
	        						i.putExtra("aLeague", leaguetoUse);
	        						i.putExtra("aTeam", teamstoUse);
	        						i.putExtra("aTime", yearstoUse);
	        						i.putExtra("businesses", bizzes);
	        						i.putExtra("sites", webSites);
	        						i.putExtra("twit", twitURL);
	        						i.putExtra("pic", imgAddy);
	        						startActivity(i);
	        						
	        						
	        						
	        						
	        						
	        					}
	        			});
	                        
	        }  
	            catch(Exception E){
                    Toast.makeText(this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                   

	            }
	                        
			
			
			
			
			
			
			
			
		}
		
		private ArrayList<Athlete> readXml(NodeList inputnodelist) {
			// TODO Auto-generated method stub
			
			try{
				ArrayList<Athlete> returnlist = new ArrayList<Athlete>();
				//Athlete[] returnlist = new Athlete[inputnodelist.getLength()];
			//	Toast.makeText(getApplicationContext(), inputnodelist.getLength() + " ", Toast.LENGTH_SHORT).show();
				int index = 0;
				String player_id;
				String first_name;
				String last_name;
				String player_name;
				String league;
				String team;
				String [] teams_input = new String[1];
				String year;
				String [] years_input = new String[1];
				String twitter;
				String achievement_tags;
				String [] achievements_input = new String[1];
				String business_name;
				String address1;
				String address2;
				String city;
				String state;
				String zip;
				String country;
				String website;
				String business_tags;
				String [] business_tags_input = new String[1];
				String imgUrl;
				
				for(int i = 0; i<inputnodelist.getLength(); i++)
				{
					//Toast.makeText(getApplicationContext(), "Made it inside the FOR Loop", Toast.LENGTH_SHORT).show();
					// Set node for this iteration
					Node athNode = inputnodelist.item(i);
					if(athNode.getNodeType() == Node.ELEMENT_NODE)
					{
						// Cast current node as an element
						Element curr_element = (Element)athNode;
						//Retrieve the id_tag and get the value it contains
						/* deprecated
						NodeList id_tag = curr_element.getElementsByTagName("player_id");
						Element id_element = (Element) id_tag.item(0);
						player_id =id_element.toString();
						*/
						//player_id = curr_element.getElementsByTagName("player_id").item(0).getTextContent();
						first_name = curr_element.getElementsByTagName("FIRST_NAME").item(0).getTextContent();
						//Toast.makeText(getApplicationContext(), first_name, Toast.LENGTH_SHORT).show();
						last_name = curr_element.getElementsByTagName("LAST_NAME").item(0).getTextContent();
						//Toast.makeText(getApplicationContext(), last_name, Toast.LENGTH_SHORT).show();
						if(curr_element.getElementsByTagName("LEAGUE").getLength() == 0)
						{
							league=" ";
						}
						else
						{
						league = curr_element.getElementsByTagName("LEAGUE").item(0).getTextContent();
						}
						//Toast.makeText(getApplicationContext(), league, Toast.LENGTH_SHORT).show();
						
						if(curr_element.getElementsByTagName("TEAM").getLength() == 0)
						{
							team = " ";
							teams_input[0] = " ";
						}
						else
						{
						team = curr_element.getElementsByTagName("TEAM").item(0).getTextContent();
						teams_input = team.split(", ");
						}
						
						
						//Toast.makeText(getApplicationContext(), team, Toast.LENGTH_SHORT).show();
						
						if(curr_element.getElementsByTagName("YEARS").getLength() == 0)
						{
							year = " ";
							years_input[0] = " ";
						}
						else
						{
						year = curr_element.getElementsByTagName("YEARS").item(0).getTextContent();
						years_input = year.split(", ");
						}
						
						//Toast.makeText(getApplicationContext(), year, Toast.LENGTH_SHORT).show();
						if(curr_element.getElementsByTagName("TWITTER").getLength() == 0)
						{
							twitter=" ";
						}
						else{
						twitter = curr_element.getElementsByTagName("TWITTER").item(0).getTextContent();
						}
						if(curr_element.getElementsByTagName("ATHLETE_IMAGE_URL").getLength() == 0)
						{
							imgUrl=" ";
						}
						else{
						imgUrl = curr_element.getElementsByTagName("ATHLETE_IMAGE_URL").item(0).getTextContent();
						}
						//Toast.makeText(getApplicationContext(), twitter, Toast.LENGTH_SHORT).show();
						
						//Toast.makeText(getApplicationContext(), " "+ curr_element.getElementsByTagName("ACHIEVEMENT_Tags").getLength() +" ", Toast.LENGTH_SHORT).show();
						if(curr_element.getElementsByTagName("ACHIEVEMENT_Tags").getLength() == 0)
						{
							achievement_tags = " ";
							achievements_input[0] = " ";
						//	Toast.makeText(getApplicationContext(), "IF", Toast.LENGTH_SHORT).show();
						}
						else
						{
						achievement_tags = curr_element.getElementsByTagName("ACHIEVEMENT_Tags").item(0).getTextContent();
						achievements_input = achievement_tags.split(", ");
						//Toast.makeText(getApplicationContext(), "ELSE", Toast.LENGTH_SHORT).show();
						}
						//Toast.makeText(getApplicationContext(), achievement_tags, Toast.LENGTH_SHORT).show();
						Business temp = new Business();
						if(curr_element.getElementsByTagName("BUSINESS_NAME").getLength() == 0)
						{
							business_name= "Name Unknown";
						}
						else{
						business_name = curr_element.getElementsByTagName("BUSINESS_NAME").item(0).getTextContent();	
						}
						//Toast.makeText(getApplicationContext(), business_name, Toast.LENGTH_SHORT).show();
						if(curr_element.getElementsByTagName("ADDRESS_1").getLength() == 0)
						{
							address1= " ";
						}
						else{
							address1 = curr_element.getElementsByTagName("ADDRESS_1").item(0).getTextContent();
						}
						
						//Toast.makeText(getApplicationContext(), address1, Toast.LENGTH_SHORT).show();
						if(curr_element.getElementsByTagName("ADDRESS_2").getLength() == 0)
						{
							address2= " ";
						}
						else{
						address2 = curr_element.getElementsByTagName("ADDRESS_2").item(0).getTextContent();
						}
						//Toast.makeText(getApplicationContext(), address2, Toast.LENGTH_SHORT).show();
						if(curr_element.getElementsByTagName("CITY").getLength() == 0)
						{
							city= " ";
						}
						else{
						city = curr_element.getElementsByTagName("CITY").item(0).getTextContent();
						}
						
						//Toast.makeText(getApplicationContext(), city, Toast.LENGTH_SHORT).show();
						if(curr_element.getElementsByTagName("STATE").getLength() == 0)
						{
							state= " ";
						}
						else{
						state = curr_element.getElementsByTagName("STATE").item(0).getTextContent();
						}
						//Toast.makeText(getApplicationContext(), state, Toast.LENGTH_SHORT).show();
						if(curr_element.getElementsByTagName("ZIP").getLength() == 0)
						{
							zip= " ";
						}
						else{
						zip = curr_element.getElementsByTagName("ZIP").item(0).getTextContent();
						}
						
						if(curr_element.getElementsByTagName("COUNTRY").getLength() == 0)
						{
							country= " ";
						}
						else{
						country = curr_element.getElementsByTagName("COUNTRY").item(0).getTextContent();
						}
						
						//Toast.makeText(getApplicationContext(), zip, Toast.LENGTH_SHORT).show();
						if(curr_element.getElementsByTagName("WEBSITE").getLength() == 0)
						{
							website= "www.inleague.com";
						}
						else{
						website = curr_element.getElementsByTagName("WEBSITE").item(0).getTextContent();
						}
						//Toast.makeText(getApplicationContext(), website, Toast.LENGTH_SHORT).show();
						if(curr_element.getElementsByTagName("Business_Tags").getLength() == 0)
						{
							business_tags= " ";
						}
						else{
						business_tags = curr_element.getElementsByTagName("Business_Tags").item(0).getTextContent();
						}
						
						//Toast.makeText(getApplicationContext(), business_tags, Toast.LENGTH_SHORT).show();
						//Now we combine the first and last name 
						player_name = first_name.concat(" "+last_name);
						//Toast.makeText(getApplicationContext(), player_name, Toast.LENGTH_SHORT).show();
						//and we must parse the strings for teams, years, achievements, and business tags via comma
						
						
						
						business_tags_input = business_tags.split(", ");
						//Now we need to dissect the businesses and build Business objects for each of them.
						ArrayList<Business> businesstopass = new ArrayList<Business> ();
						//businesstopass.add(new Business(business_name, address1, address2, city, state, zip, ));
						temp.setName(business_name);
						temp.setAddress1(address1);
						temp.setAddress2(address2);
						temp.setCity(city);
						temp.setState(state);
						temp.setZip(zip);
						temp.setCountry(country);
						temp.setWebsite(website);
						temp.setBizTags(business_tags);
						
						businesstopass.add(temp);
						
						
						//Toast.makeText(getApplicationContext(), player_name, Toast.LENGTH_SHORT).show();
						//now that all that crap is done, we can create the athlete object
						Athlete guy=new Athlete(player_name, league, teams_input, years_input, twitter, achievements_input, businesstopass, imgUrl);
						int o=0;
						int j=0;
						int a=0;
						while(o<returnlist.size()){
							if(returnlist.get(o).equals(guy)){ 
								j++;
								a=o;
							}
							o++;
						}
						if(j == 0){
						returnlist.add(guy);
						}
						else{
							returnlist.get(a).addBusiness(temp);
						}
						//returnlist[i] = new Athlete(player_name, league, teams_input, years_input, twitter, achievements_input, businesstopass);
						//if(i >= 30)
							//Toast.makeText(getApplicationContext(), player_name + i, Toast.LENGTH_SHORT).show();
						
						
					}
					
					
					
				}
				
				return returnlist;
				
				
				
			}
			catch(Exception e)
			{
				throw new RuntimeException("Error occured during reading Athlete Data",e);
			}
			
			

		}
}
