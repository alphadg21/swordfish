package com.inleague.swordfish;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class League extends Activity {

	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teamlist);
        
        getActionBar().setDisplayHomeAsUpEnabled(true);
        String whichToUse=getIntent().getStringExtra("league");
        String[] theseTeams = null;
        ListView putHere=(ListView)findViewById(R.id.teamlist);

        if(whichToUse.equals("NBA")){
        theseTeams=getResources().getStringArray(R.array.NBA);
        ArrayAdapter<String> makeItHappen=new ArrayAdapter<String>(this, R.layout.list_item, theseTeams);
        putHere.setAdapter(makeItHappen);
        makeItHappen.notifyDataSetChanged();
        }
        if(whichToUse.equals("NFL")){
            theseTeams=getResources().getStringArray(R.array.NFL);
            ArrayAdapter<String> makeItHappen=new ArrayAdapter<String>(this, R.layout.list_item, theseTeams);
            putHere.setAdapter(makeItHappen);
            makeItHappen.notifyDataSetChanged();
            }
        if(whichToUse.equals("MLB")){
            theseTeams=getResources().getStringArray(R.array.MLB);
            ArrayAdapter<String> makeItHappen=new ArrayAdapter<String>(this, R.layout.list_item, theseTeams);
            putHere.setAdapter(makeItHappen);
            makeItHappen.notifyDataSetChanged();
            }
        if(whichToUse.equals("NHL")){
            theseTeams=getResources().getStringArray(R.array.NHL);
            ArrayAdapter<String> makeItHappen=new ArrayAdapter<String>(this, R.layout.list_item, theseTeams);
            putHere.setAdapter(makeItHappen);
            makeItHappen.notifyDataSetChanged();
            }
        final String[] goFromHere=theseTeams;
        putHere.setOnItemClickListener(new OnItemClickListener(){
        	
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent goTeam=new Intent(League.this, TeamListForever.class);
				goTeam.putExtra("teamtext",goFromHere[arg2]);
				startActivity(goTeam);
				
			}
        	
        });
	}

}
