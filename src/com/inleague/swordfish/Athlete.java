package com.inleague.swordfish;

import java.util.ArrayList;

public class Athlete {

	String id;
	String name;
	String league;
	String [] teams;
	String [] years;
	String twitter;
	String [] achievements;
	String imgUrl;
	
	ArrayList<Business> bizness = new ArrayList<Business>();
	int achievenum = 0;
	int teamnum = 0;
	int yearnum = 0;
	int biznum = 1;
	
	public Athlete()
	{
		id = "not_currently_assigned";
		name = "Name is currently unlisted";
		league = "Professional_Shuffle_Boarding";
		teams[0] = "Team affiliations are currently unlisted";
		years[0] = "Years are currently unlisted";
		twitter = "https://twitter.com/";
		achievements[0] = "Achievements are currently unlisted";
		bizness.add(new Business());
		imgUrl=" ";
	}
	
	public Athlete(String aname, String aleague, String[] ateams, String[] ayears, String atwitter, String [] aachievements, ArrayList<Business> multiplebusinesses, String anImage)
	{
		name = aname;
		league = aleague;
		teams = ateams;
		teamnum = teams.length;
		years = ayears;
		yearnum = years.length;
		if(atwitter != null)
		{
			twitter= atwitter;
		}else
		twitter = " ";
		
		if(aachievements != null)
		{
			achievements = aachievements;
			achievenum = achievements.length;
		}else
		achievements[0] = "Achievemenets are currently unlisted";
		
		if(multiplebusinesses != null)
		{
			
			bizness = multiplebusinesses;
		}else
		bizness.add(new Business("not_affiliated"));
		imgUrl=anImage;
		
		
	}
	//constructor for when we only have name, league, teams, and years
	public Athlete(String aname, String aleague, String[] ateams, String[] ayears)
	{
		name = aname;
		league = aleague;
		teams = ateams;
		teamnum = teams.length;
		years = ayears;
		yearnum = years.length;
		twitter = " ";
		achievements[0] = "Achievemenets are currently unlisted";
		bizness.add(new Business("not_affiliated"));
	}
	
	public void setName(String newname)
	{
		name = newname;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setLeague(String newleague)
	{
		league = newleague;
	}
	public String getLeague()
	{
		return league;
	}
	
	/*
	 * 
	 */
	public void addAchievement(String newachievement)
	{
		achievements[achievenum] = newachievement;
		achievenum++;
	}
	public void setAchievement(String achievementtoset, int indextoplaceat)
	{
		achievements[indextoplaceat] = achievementtoset;
	}
	
	public String [] getAchievements()
	{
		return achievements;
	}
	
	public String getSpecificAchievement(int indexachievementat)
	{
		return achievements[indexachievementat];
	}
	/*
	 * 
	 */
	public void addTeam(String newteam)
	{
		teams[teamnum] = newteam;
		teamnum++;
	}
	public void setTeam(String teamtoset, int indextoplaceat)
	{
		teams[indextoplaceat] = teamtoset;
	}
	
	public String [] getTeams()
	{
		return teams;
	}
	
	public String getSpecificTeam(int indexteamat)
	{
		return teams[indexteamat];
	}
	/*
	 * 
	 */
	public void addYear(String newyear)
	{
		years[yearnum] = newyear;
		yearnum++;
	}
	public void setYear(String yeartoset, int indextoplaceat)
	{
		years[indextoplaceat] = yeartoset;
	}
	
	public String [] getYears()
	{
		return years;
	}
	
	public String getSpecificYear(int indexyearat)
	{
		return years[indexyearat];
	}
	/*
	 * 
	 * 
	 */
	public void addBusiness(Business newbiz)
	{
		bizness.add(newbiz);
		biznum++;
	}
	/*
	public void setBusiness(Business biztoset, int indextoplaceat)
	{
		bizness[indextoplaceat] = biztoset;
	}
	*/
	public Business getBusinesses()
	{
		return bizness.get(0);
	}
	public Business getBusiness(int get)
	{
		if(get<=biznum){
		return bizness.get(get);
		}
		else{
			return null;
		}
	}
	/*
	public Business getSpecificBusiness(int indexbizat)
	{
		return bizness[indexbizat];
	}
	*/
	public void setTwitter(String newtwitteracct)
	{
		twitter = newtwitteracct;
	}
	public String getTwitter()
	{
		return twitter;
	}
	public void setImageUrl(String newimg)
	{
		imgUrl = newimg;
	}
	public String getImageUrl()
	{
		return imgUrl;
	}
	
	public boolean equals(Athlete tocheck)
	{
		if(tocheck.getName().equals(name) && tocheck.getLeague().equals(league) && tocheck.getTeams().length == teams.length){
			return true;
		}
		else
			return false;
		
	}
}