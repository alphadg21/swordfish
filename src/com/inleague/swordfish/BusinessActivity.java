package com.inleague.swordfish;

import java.io.IOException;
import java.util.List;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class BusinessActivity  extends FragmentActivity{
	
	private GoogleMap mMap;
    private UiSettings mUiSettings;
    public static final int  MAP_TYPE_HYBRID = 4;
    String addyToMark;
    String bizName;
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.businessactivity);
        
        getActionBar().setDisplayHomeAsUpEnabled(true);
        addyToMark=getIntent().getStringExtra("spot");
        bizName=getIntent().getStringExtra("title");
        TextView nameSpace=(TextView)findViewById(R.id.myBizName);
        TextView addySpace=(TextView)findViewById(R.id.myBizAddy);
        nameSpace.setText(bizName);
        addySpace.setText(addyToMark);
        Button getHereButton=(Button)findViewById(R.id.getThereButton);
        getHereButton.setText("Get Directions");
        Geocoder myGc=new Geocoder(getBaseContext());
        List<Address> address;
        LocationManager lm=(LocationManager)getSystemService(Context.LOCATION_SERVICE);
        final Location mine=lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        try {
			address = myGc.getFromLocationName(addyToMark,1);
			final Address location = address.get(0);
		
	    
        getHereButton.setOnClickListener(new OnClickListener(){
        	
			@Override
			public void onClick(View arg0) {
				final Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("http://maps.google.com/maps?" + "saddr="+ mine.getLatitude() + "," + mine.getLongitude() + "&daddr=" + location.getLatitude() + "," + location.getLongitude()));
			    intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
			                        startActivity(intent);
				
			}
        	
        });
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        setUpMapIfNeeded();
    }
    

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        if (mMap != null) {
            updateMyLocation();
        }
    }    
    
    
    public void onMyLocationToggled(View view) {
    	Toast.makeText(getApplicationContext(), "onMyLocationToggled ", Toast.LENGTH_SHORT).show();
    	updateMyLocation();
    }

    private void updateMyLocation() {
    	
        mMap.setMyLocationEnabled(true);
        mUiSettings.setMyLocationButtonEnabled(true);
        Location current = mMap.getMyLocation();
       if(current != null)
        Toast.makeText(getApplicationContext(), "Latitude: "+current.getLatitude()+"\nLongitude: "+current.getLongitude(), Toast.LENGTH_LONG).show();
    }


    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
           mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                
                mMap.setMyLocationEnabled(true);
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                setUpMap();
                
            }
        }
    }

    private void setUpMap() {
        mUiSettings = mMap.getUiSettings();
        try {
			MapsInitializer.initialize(getBaseContext());
		
        
        Geocoder myGc=new Geocoder(getBaseContext());
        List<Address> address;

    	
	   
        address = myGc.getFromLocationName(addyToMark,1);
		
	    
	    
	    Address location = address.get(0);
	    LatLng toUse=new LatLng(location.getLatitude(),location.getLongitude());
	    mMap.addMarker(new MarkerOptions().position(toUse).title(bizName));
	    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(toUse, 12));
        } catch (Exception e) {
			e.printStackTrace();
		}
        
        
    
    }
}
