package com.inleague.swordfish;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.fima.cardsui.objects.Card;

public class LocationCard extends Card {

	

	public LocationCard(String title, String description){
		super(title, description);
	}

	@Override
	public View getCardContent(Context context) {
		View view = LayoutInflater.from(context).inflate(R.layout.card, null);

		((TextView) view.findViewById(R.id.title)).setText(title);
		((TextView) view.findViewById(R.id.description)).setText(desc);
		
		return view;
	}

    @Override
    public boolean convert(View convertCardView) {
        return false;
    }


}
