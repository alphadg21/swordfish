package com.inleague.swordfish;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.widget.Toast;

public class TeamYear {
	private String team;
	private String years;
	int[] yearsArray;
	
	public TeamYear(String aTeam, String yearsSet, int[] yearsinSet) {
        team=aTeam;
        years=yearsSet;
        yearsArray=yearsinSet;
}
	public TeamYear(String aTeam, String yearsSet) {
        team=aTeam;
        years=yearsSet;
        setYearsNos(yearsSet);
        
}
public TeamYear() {
    team=" ";
    years=" ";
    yearsArray=new int[1];
}


public String getTeam() {
        return team;
}
public void setTeam(String newTeam) {
        this.team = newTeam;
}
public String getYrString() {
        return years;
}
public void setYrString(String aSet) {
        this.years=aSet;
}
public int[] getYearsNos() {
        return yearsArray;
}
@SuppressLint("DefaultLocale")
public void setYearsNos(String setOfYears) {
        if(setOfYears.length()<5 && setOfYears.length()>3){
        	int oneYear=Integer.parseInt(setOfYears);
        	int[] thisYear=new int[1];
        	thisYear[0]=oneYear;
        	yearsArray=thisYear;
        }
        else if(setOfYears.equals(" ")){
        	int[] na=new int[1];
        	na[0]=0;
        	yearsArray=na;
        }
        else{
        	String[] twoEnds=setOfYears.split("-");
        	String start=twoEnds[0].trim();
        	int beg=Integer.parseInt(start);
        	String finish="";
        	if(!twoEnds[1].trim().toLowerCase(Locale.ENGLISH).equals("present")){
        	finish=twoEnds[1].trim();
        	}
        	else{
        		finish=Integer.toString(Calendar.getInstance().get(Calendar.YEAR));
        	}
        	int end=Integer.parseInt(finish);
        	int big=end-beg;
        	int[] yearsBunch=new int[big+1];
        	int i=0;
        	while(i<yearsBunch.length){
        		yearsBunch[i]=beg+i;
        		i++;
        		
        	}
        	yearsArray=yearsBunch;
        }
}

}
