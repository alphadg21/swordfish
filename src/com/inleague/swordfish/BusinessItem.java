package com.inleague.swordfish;

public class BusinessItem {
	private String name;
    private String web;
    private String address;
    
    public BusinessItem(String Name, String URL, String nuAddress) {
            super();
            this.name = Name;
            this.web = URL;
            this.address = nuAddress;
    }
    public BusinessItem() {
        name=" ";
        web=" ";
        address=" ";
}
    
    // Getter and setter methods for all the fields.
    // Though you would not be using the setters for this example,
    // it might be useful later.
    public String getName() {
            return name;
    }
    public void setName(String newName) {
            this.name = newName;
    }
    public String getURL() {
            return web;
    }
    public void setWeb(String newSite) {
            this.web = newSite;
    }
    public String getAddy() {
            return address;
    }
    public void setAddy(String newPlace) {
            this.address = newPlace;
    }
}
