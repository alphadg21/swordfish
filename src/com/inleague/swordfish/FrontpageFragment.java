package com.inleague.swordfish;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Intent;
import android.widget.*;
import com.parse.ParseQuery;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import com.parse.ParseObject;
import com.parse.*;


public class FrontpageFragment extends Fragment{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    public static final String ARG_SECTION_NUMBER = "0";

    public FrontpageFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.listfragment, container, false);


        ParseQuery<ParseObject> mySet=ParseQuery.getQuery("NFLAthletes");
        mySet.setLimit(10);
        mySet.orderByDescending("viewCount");
        mySet.selectKeys(Arrays.asList("firstName", "lastName", "athleteImageUrl"));

        try {
        final List<ParseObject> frontPlayers=mySet.find();

            final ArrayList<AthleteTile> letsGo = new ArrayList<AthleteTile>();
            int i=0;
            while(i<10){

                AthleteTile temp=new AthleteTile();
                temp.setName(frontPlayers.get(i).get("firstName") + " " + frontPlayers.get(i).get("lastName"));
                temp.setUrl(frontPlayers.get(i).getString("athleteImageUrl"));
                letsGo.add(temp);
                i++;
            }

            ArrayAdapter<AthleteTile> aa=new ArrayAdapter<AthleteTile>(getActivity(), R.id.gridfrag,letsGo){
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v=convertView;

                    final AthleteTile thisDude=letsGo.get(position);
                    if (v == null) {
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        v = inflater.inflate(R.layout.frontpageitem, parent, false);
                    }
                    TextView athName = (TextView) v.findViewById(R.id.myImageViewText);
                    ImageView athImage = (ImageView) v.findViewById(R.id.myImageView);

                    athName.setText(thisDude.getName());
                    if(!thisDude.getUrl().equals(" ")){
                        UrlImageViewHelper.setUrlDrawable(athImage, thisDude.getUrl());
                    }
                    return v;
                }
            };

            GridView athleteView = (GridView) rootView.findViewById(R.id.gridfrag);

            athleteView.setAdapter(aa);
            aa.notifyDataSetChanged();
            Toast.makeText(getActivity(), frontPlayers.get(0).getObjectId(), Toast.LENGTH_LONG).show();
            athleteView.setOnItemClickListener(new OnItemClickListener(){

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Intent I=new Intent(getActivity(), AthleteActivity.class);
                    I.putExtra("number", frontPlayers.get(position).getObjectId());
                    startActivity(I);
                }
            });



        } catch (ParseException e) {
            e.printStackTrace();
        }

        return rootView;
}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
    }
}
