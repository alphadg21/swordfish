package com.inleague.swordfish;

public class AthleteTile {
	
	private String name;
	private String url;
	
public AthleteTile(String Name, String addy) {
        super();
        this.name = Name;
        this.url= addy;
}

public AthleteTile() {
    name=" ";
}

public String getName() {
        return name;
}
public void setName(String newName) {
        this.name = newName;
}
public String getUrl() {
    return url;
}
public void setUrl(String newAddy) {
    this.url=newAddy;
}
}
