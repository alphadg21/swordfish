package com.inleague.swordfish;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.parse.*;


public class AthleteActivity extends Activity{
	Context context=this;
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.athleteactivity);
        
        getActionBar().setDisplayHomeAsUpEnabled(true);

        String id=getIntent().getStringExtra("number");
        ParseQuery<ParseObject> query = ParseQuery.getQuery("NFLAthletes");
        ParseObject myAthlete=null;

        try {
            myAthlete=query.get(id);
        } catch (ParseException e) {
            e.printStackTrace();
        }




        String myName=myAthlete.get("firstName")+" "+myAthlete.get("lastName");
        final String myLeague=myAthlete.getString("LEAGUE");
        String[] myTeams=myAthlete.getString("TEAM").split(", ");
        String[] myYears=myAthlete.getString("YEARS").split(", ");
        String myTwit=myAthlete.getString("TWITTER");
        if(myTwit==null){
            myTwit=" ";
        }
        String myPic=myAthlete.getString("athleteImageUrl");
        Toast.makeText(this, myPic, Toast.LENGTH_SHORT).show();
        String checkURL="http";
        ImageView twitIc=(ImageView) findViewById(R.id.imageViewTwit);
        if(!myTwit.equals(" ")){
        twitIc.setVisibility(View.VISIBLE);
        String newTwit=myTwit;
    	if(!myTwit.substring(0,4).equals(checkURL)){
    		String add="http://";
    		String put=add.concat(myTwit);
    		newTwit=put;
    	}
        
    	final String twittoUse=newTwit;
        Toast.makeText(this, twittoUse,Toast.LENGTH_LONG).show();
        
        twitIc.setOnClickListener(new OnClickListener(){
        	
			@Override
			public void onClick(View arg0) {
				
				Intent I=new Intent(Intent.ACTION_VIEW, Uri.parse(twittoUse));
				startActivity(I);
			}
        	
        }); 
        }
        if(!myPic.equals(" ")){
        	ImageView athPic=(ImageView)findViewById(R.id.imageView1);
        	UrlImageViewHelper.setUrlDrawable(athPic, myPic);
        }
        int i=0;
        final ArrayList<TeamYear> relatedList=new ArrayList<TeamYear>();
        String teamCheck=myTeams[0];
        
        
        ArrayAdapter<TeamYear> adaptThat=new ArrayAdapter<TeamYear>(getBaseContext(), R.id.yearsList, relatedList){
            
            
    		@Override 
            public View getView(int position, View convertView, ViewGroup parent) {
                View v=convertView;
                final TeamYear thisTime=relatedList.get(position);
                
                if (v == null) {
                    LayoutInflater inflater = getLayoutInflater();
                    v = inflater.inflate(R.layout.yearsitem, parent, false);
                }
                TextView team = (TextView) v.findViewById(R.id.teamText);
                team.setText(thisTime.getTeam());

                TextView year = (TextView) v.findViewById(R.id.yearText);
                year.setText(thisTime.getYrString());
                
        
                return v;
    		}
        };

        while(i<(myTeams.length)){
        	String aTeam=myTeams[i];
        	String aYearSet;
        	if(!myYears[0].equals(" ")){
        	aYearSet=myYears[i];
        	}
        	else{
        		aYearSet=" ";
        	}
        	TeamYear temp=new TeamYear(aTeam, aYearSet);
        	relatedList.add(temp);
        	i++;
        }
        
        ListView yrListView = (ListView)findViewById(R.id.yearsList);
        if(!relatedList.get(0).getTeam().equals(" ")){
        	yrListView.setVisibility(View.VISIBLE);
        }
		yrListView.setAdapter(adaptThat);
		adaptThat.notifyDataSetChanged();
		yrListView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				final Dialog tmDialog=new Dialog(context);
				tmDialog.setContentView(R.layout.dialogyears);
				tmDialog.setTitle("Teams");
				final ListView teamList=(ListView)tmDialog.findViewById(R.id.yearsPick);
				ArrayList<String> teampicks=new ArrayList<String>();
				int j=0;
				while(j<relatedList.size()){
					teampicks.add(relatedList.get(j).getTeam());
					j++;
				}
				ArrayAdapter<String> adaptTms=new ArrayAdapter<String>(context, R.layout.list_item,teampicks);
				teamList.setAdapter(adaptTms);
				adaptTms.notifyDataSetChanged();
				
				teamList.setOnItemClickListener(new OnItemClickListener(){

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						
						final TeamYear picked=relatedList.get(arg2);
						picked.setYearsNos(picked.getYrString());
						int[] pickThese=picked.getYearsNos();
						
						final Dialog yrDialog=new Dialog(context);
						yrDialog.setContentView(R.layout.dialogyears);
						yrDialog.setTitle(picked.getTeam()+" "+picked.getYrString());
						final ListView dialList=(ListView)yrDialog.findViewById(R.id.yearsPick);
						ArrayList<String> stringThese=new ArrayList<String>();
						int i=0;
						while(i<pickThese.length){
							stringThese.add(Integer.toString(pickThese[i]));
							i++;
						}
						ArrayAdapter<String> adaptYrs=new ArrayAdapter<String>(getBaseContext(), R.layout.list_item, stringThese);
						dialList.setAdapter(adaptYrs);
						adaptYrs.notifyDataSetChanged();
						dialList.setOnItemClickListener(new OnItemClickListener(){

							@Override
							public void onItemClick(AdapterView<?> arg0, View arg1,
									int arg2, long arg3) {
								String playerTeam=picked.getTeam();
								String pickYear=(String) dialList.getItemAtPosition(arg2);
								Intent listem=new Intent(AthleteActivity.this, TeamListView.class);
								listem.putExtra("team", playerTeam);
								listem.putExtra("year", pickYear);
								startActivity(listem);
							}
							
						});
						yrDialog.show();
					}
					
				});
				tmDialog.show();
				
				
			}

			
			
		}); 

        TextView athName=(TextView)findViewById(R.id.textView1);
        TextView athLeague=(TextView)findViewById(R.id.textView2);
        athName.setText(myName);
        athLeague.setText(myLeague);
        athLeague.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent goToLeague=new Intent(AthleteActivity.this,League.class);
				goToLeague.putExtra("league", myLeague);
				startActivity(goToLeague);
			}
        });
        final ArrayList<BusinessItem> places=new ArrayList<BusinessItem>();
        
        
        ArrayAdapter<BusinessItem> adaptThis=new ArrayAdapter<BusinessItem>(getBaseContext(), R.id.bizList, places){
        
        
		@Override 
        public View getView(int position, View convertView, ViewGroup parent) {
            View v=convertView;
            final BusinessItem thisPlace=places.get(position);
            if (v == null) {
                LayoutInflater inflater = getLayoutInflater();
                v = inflater.inflate(R.layout.list_item2, parent, false);
            }
            TextView busName = (TextView) v.findViewById(R.id.busName);
            busName.setText(thisPlace.getName());

            TextView busAddr = (TextView) v.findViewById(R.id.busAddr);
            busAddr.setText(thisPlace.getAddy());
            
            
            if(!thisPlace.getAddy().equals(" ")){
                ImageView mark =(ImageView)v.findViewById(R.id.markerView);
                mark.setVisibility(0);
            
            mark.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View arg0) {
					
					Intent I=new Intent(getBaseContext(), BusinessActivity.class);
					I.putExtra("title", thisPlace.getName());
					I.putExtra("spot", thisPlace.getAddy());
					startActivity(I);
				}
            	
            }); 
            }

            busName.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View arg0) {
					String webAddress=thisPlace.getURL();
					Intent I=new Intent(Intent.ACTION_VIEW, Uri.parse(webAddress));
					startActivity(I);
				}
            	
            }); 

			return v;
            
        }
        };
        
        int n=0;

        //while(n<businesses.size()){
        	String thisBiz=myAthlete.getString("businessName");
        String anAddy1=myAthlete.getString("address1");
        thisBiz=thisBiz.concat("\n");
            if(anAddy1!=null){
            thisBiz=thisBiz.concat(anAddy1);
            }
        String anAddy2=myAthlete.getString("address2");
        if(anAddy2!=null){
            thisBiz=thisBiz.concat(anAddy2);
        }
        thisBiz=thisBiz.concat(" "+"\n");
        String aCity=myAthlete.getString("CITY");
        if(aCity!=null){
            thisBiz=thisBiz.concat(aCity);
        }
        String aState=myAthlete.getString("STATE");
        if(aState!=null){
            thisBiz=thisBiz.concat(", "+aState);
        }
        String aCountry=myAthlete.getString("COUNTRY");
        if(aCountry!=null){
            thisBiz=thisBiz.concat(" "+aCountry);
        }
        thisBiz=thisBiz.concat(" ");

            String thisURL=" ";
            if(myAthlete.getString("WEBSITE")!=null){
                thisURL=myAthlete.getString("WEBSITE");
            };
        	if(!thisURL.substring(0,4).equals(checkURL)){
        		String add="http://";
        		String put=add.concat(thisURL);
        		thisURL=put;
        	}
        	
        	String[] splitUp=thisBiz.split("\n");
        	String thisName=splitUp[0];
        	String thisAddy=splitUp[1];
        	thisAddy+="\n" + splitUp[2];
        	BusinessItem toAdd=new BusinessItem(thisName, thisURL, thisAddy);
        	places.add(toAdd);
        	n++;
        //}


		ListView bizListView = (ListView)findViewById(R.id.bizList);
		bizListView.setAdapter(adaptThis);
		adaptThis.notifyDataSetChanged();




    }

	}

