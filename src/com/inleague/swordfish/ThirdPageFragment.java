package com.inleague.swordfish;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fima.cardsui.views.CardUI;



public class ThirdPageFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    public static final String ARG_SECTION_NUMBER = "0";

    public ThirdPageFragment() {
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
            Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.searchfragment, container, false);
        MyCard searchbar=new MyCard("Search By Player Name");
        searchbar.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				Intent I=new Intent(getActivity(), SearchActivity.class);
				startActivity(I);
				
			}
        	
        });
        final MyCard NBAcard=new MyCard("NBA");

        NBAcard.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent goToLeague=new Intent(getActivity(),League.class);
				goToLeague.putExtra("league", NBAcard.getTitle());
				startActivity(goToLeague);
			}
        });
        final MyCard NFLcard=new MyCard("NFL");
        NFLcard.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent goToLeague=new Intent(getActivity(),League.class);
				goToLeague.putExtra("league", NFLcard.getTitle());
				startActivity(goToLeague);
			}
        });
        final MyCard MLBcard=new MyCard("MLB");
        MLBcard.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent goToLeague=new Intent(getActivity(),League.class);
				goToLeague.putExtra("league", MLBcard.getTitle());
				startActivity(goToLeague);
			}
        });
        final MyCard NHLcard=new MyCard("NHL");
        NHLcard.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent goToLeague=new Intent(getActivity(),League.class);
				goToLeague.putExtra("league", NHLcard.getTitle());
				startActivity(goToLeague);
			}
        });
        
        
        
        
        
        
        
        
        
        
        final CardUI myCards=(CardUI)rootView.findViewById(R.id.cardsview);
        myCards.setSwipeable(false);

        myCards.addCard(searchbar);
        myCards.addCard(NFLcard);
        myCards.addCard(NBAcard);
        myCards.addCard(NHLcard);
        myCards.addCard(MLBcard);
        
        myCards.refresh();
        
        return rootView;
    }
		

	}
