package com.inleague.swordfish;

import com.google.android.gms.maps.model.LatLng;

public class Business {

	String name;
	String address1;
	String address2;
	String city;
	String state;
	String zip;
	String country;
	String website;
	String business_tags;
	LatLng location;
	Athlete [] athletes;
	int athletenum = 0;
	
	/*
	 * Default constructor for the Business class, gives the Business
	 * random default values to be modified later if they are currently unknown
	 */
	public Business()
	{
	name = "Default Name";
	address1 = "Default Name";
	address2 = "123 Anywhere St";
	city = "Illinois";
	zip = "60632";
	country = "USA! USA!";
	website = "inleague.com";
	business_tags = " ";
	// location = new LatLng(0,0);
	athletes = new Athlete[] {};
	
	}
	/*
	 * Non-default constructor, to be used when all applicable information is
	 * known about the business and to be passed in to create the full object
	 */
	public Business(String aname, String aaddress1, String aaddress2, String acity, String azip, String awebsite, String abiztag, LatLng alocation, Athlete [] aathlete)
	{
		name =aname;
		address1 = aaddress1;
		address2 = aaddress2;
		city = acity;
		zip = azip;
		website = awebsite;
		business_tags = abiztag;
		location = alocation;
		athletes = aathlete;
		athletenum = athletes.length;
	}

	public Business(String string) {
		// TODO Auto-generated constructor stub
		if(string == "not_affiliated")
		{
			name = "No businesses affiliated with this Athlete";

			location = new LatLng(0,0);


			website = "inleague.com";
			
		}
	}
	/*
	 * Adds the passed in athlete to the last position in the array, as
	 * determined by the athletenum value, then advances the athletenum
	 * since the number of associated athletes has increased
	 */
	public void addAthlete(Athlete athletetoadd)
	{
		athletes[athletenum] = athletetoadd;
		athletenum++;
	}
	//Replaces the athlete at the given index with the given athlete
	public void setAthlete(Athlete athletetoset, int indextoplaceat)
	{
		athletes[indextoplaceat] = athletetoset;
	}
	//Returns all the athletes associated with the business
	public Athlete[] getAthletes()
	{
		return athletes;
	}
	//Returns the athlete at the specified index
	public Athlete getSpecificAthlete(int indexathleteat)
	{
		return athletes[indexathleteat];
	}
	
	public void setWebsite(String aweb)
	{
		website = aweb;
	}
	
	public String getWebsite()
	{
		return website;
	}

	public void setLocation(LatLng newloc)
	{
		location = newloc;
	}
	
	public LatLng getLocation()
	{
		return location;
	}
	
	public void setName(String newname)
	{
		name = newname;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setAddress1(String newaddress1)
	{
		address1 = newaddress1;
	}
	public String getAddress1()
	{
		return address1;
	}
	
	public void setAddress2(String newaddress2)
	{
		address2 = newaddress2;
	}
	public String getAddress2()
	{
		return address2;
	}
	
	public void setCity(String newcity)
	{
		city = newcity;
	}
	public String getCity()
	{
		return city;
	}
	
	public void setState(String newstate)
	{
		state = newstate;
	}
	public String getState()
	{
		return state;
	}
	
	public void setZip(String newzip)
	{
		zip = newzip;
	}
	public String getZip()
	{
		return zip;
	}
	
	public void setCountry(String newcountry)
	{
		country = newcountry;
	}
	public String getCountry()
	{
		return country;
	}
	
	public void setBizTags(String newtags)
	{
		business_tags = newtags;
	}
	public String getTags()
	{
		return business_tags;
	}
	@Override
	public String toString(){
		String printit=name+"\n"+address1+" "+address2+"\n"+city+" "+state+" "+zip+" "+country;
		return printit;
	}
	
	
}
